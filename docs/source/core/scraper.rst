Scraper
=======
Module ``richy.core.scraper`` contains classes responsible for actuall
web content scraping.

Manager
-------
Class ``Manager`` handles all the methods for scraping actuall items:

.. literalinclude:: ../../../app/richy/core/scraper.py
   :pyobject: Manager
