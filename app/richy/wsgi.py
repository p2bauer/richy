"""
WSGI config for richy project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

if "true" == os.environ.get("DJANGO_DEBUG"):
    settings = "richy.settings.dev"
else:
    settings = "richy.settings.base"

os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings)

application = get_wsgi_application()
