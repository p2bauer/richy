#!/bin/sh
./manage.py dumpdata --indent=4 --natural-foreign --natural-primary \
    sitetree.Tree \
    core.NavigationTreeItem \
    > richy/core/fixtures/sitetree.json
./manage.py dumpdata --indent=4 --natural-foreign --natural-primary \
    core.Meta \
    > richy/core/fixtures/meta.json
./manage.py dumpdata --indent=4 django_celery_beat > richy/core/fixtures/celery_beat.json
./manage.py dumpdata --indent=4 core.User > richy/core/fixtures/users.json
./manage.py dumpdata --indent=4 --natural-primary transactions.exchange \
    > richy/transactions/fixtures/exchanges.json
