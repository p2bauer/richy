from django.contrib import admin

from ..core.admin import ItemMixin
from .models import Coin


@admin.register(Coin)
class CoinAdmin(ItemMixin, admin.ModelAdmin):
    pass
