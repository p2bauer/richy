from django.apps import AppConfig


class EtfsConfig(AppConfig):
    name = "richy.etfs"
