ItemChart
=========
Basic chart for showing price line.

Chart has 2 modes. Value and percentage. In value chart shows
classic price values over time. In percentage chart shows price
in percentage where the base (0%) is the currently first visible
price point.

Chart can also show SMA 20/50/200 and transaction bubles. Positive
(buy) transaction are shown as bubbles with "+" sign and negative
transactions with "-" sign.

Custom range selectors can be added to the chart (see top left corner
of the chart). There are default buttons for 1m, 3m, 6m, YTD and All.
Great example is to add range since last transaction (if any). In that
case ``ranges`` button will be following json:

.. code:: javascript

    {
        title: "LT",  // Last transaction
        from: 1500933600000,
        to: 1528416000000
    }

.. figure:: imgs/item_chart.png
   :align: center

   Example percentage chart with transactions and custom range selector

It's also possible draw plot lines which are straight lines across the chart.
These lines are part of the async request data response from the server
as ``plotLines`` (a list).

.. code:: python

   data["plotLines"] = [
       {
           "value": 5,
           "color": "#dc0000",
           "width": 2,
           "zIndex": 4,
           "label": {"text": "my plot line"},
       }
   ]


Component accepts folllowing params:

* ``symbol`` - Symbol of the item (required).
* ``index``
* ``transactions`` - Boolean flag - determines whether chart should
  also show transaction flags (default false).
* ``smas`` - Boolean flag - determines whether chart should also
  show Small Moving Averages (20, 50, 200) (default false).
* ``percents`` - Boolean flag - determines whether chart axis should
  be in percents - relative to minimal extreme (first visible point)
  (default false).
* ``hidden`` - Boolean flag - visibility of chart (default false).
* ``multipleSeries`` - Boolean flag - determines whether the chart will
  draw multiple series (default false).
* ``ranges`` - array of objects where each object has ``title``
  ``from`` and ``to`` properties. ``from`` and ``to`` are in
  microseconds.
* ``optionOverrides`` - an object which items overrides default chart options
