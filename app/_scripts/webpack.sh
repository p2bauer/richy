#!/bin/bash

docker compose -f docker-compose.dev.yaml up webpack
docker compose -f docker-compose.dev.yaml exec richy ./manage.py collectstatic -i node_modules --no-input
