"""
A group of common tools for templates.
"""

import json
import logging
import random
import string
from datetime import datetime
from decimal import Decimal
from pathlib import Path

import numpy as np
import pandas as pd
from django import template
from django.conf import settings
from django.contrib.humanize.templatetags.humanize import intcomma
from django.forms import CheckboxInput, RadioSelect
from django.forms.widgets import FileInput, Select, SelectMultiple

LOGGER = logging.getLogger(__name__)
register = template.Library()


@register.simple_tag
def random_str(length=5):
    """
    Generates random string from [A-Z0-9] choice in the given length.

    :return: Generated string.
    :rtype: str
    """

    return "".join(
        random.choice(string.ascii_uppercase + string.digits) for _ in range(length)
    )


@register.filter
def is_checkbox(field):
    """
    Checks if the given object is instance of form checkbox field.
    Used in ``_form_field.pug`` template.

    :return: True if the given field is checkbox.
    :rtype: bool
    """

    try:
        return (
            field.field.widget.__class__.__name__ == CheckboxInput().__class__.__name__
        )
    except:
        pass


@register.filter
def is_select(field):
    """
    Checks if the given object is instance of form select field.
    Used in ``_form_field.pug`` template.

    :return: True if the given field is select.
    :rtype: bool
    """

    return isinstance(field.field.widget, Select) and not isinstance(
        field.field.widget, SelectMultiple
    )


@register.filter
def is_file(field):
    """
    Checks if the given object is instance of form file field.
    Used in ``_form_field.pug`` template.

    :return: True if the given field is file field.
    :rtype: bool
    """

    return isinstance(field.field.widget, FileInput)


@register.filter
def is_multi_select(field):
    """
    Checks if the given object is instance of form field widget is multi-select.
    Used in ``_form_field.pug`` template.

    :return: True if the given field is multi-select.
    :rtype: bool
    """

    return isinstance(field.field.widget, SelectMultiple)


@register.filter
def is_radio_choice(field):
    """
    Checks if the given object is instance of form field widget is radio select.
    Used in ``_form_field.pug`` template.

    :return: True if the given field is radio select.
    :rtype: bool
    """

    return isinstance(field.field.widget, RadioSelect)


@register.filter
def autofloatformat(value, no_str=False):
    """
    Rounds number to ``settings.MAX_PRECISION`` decimal places
    (maximum precision we care about) with respect to number value.

    Numbers are rounded in a way that at least 2 decimal places are present
    with respect to preceding zeroes to real value.

    1.234 can be safely rounded to 1.23 (2 decimal places).
    0.1234 can be safely rounded to 0.123 (3 decimal places).
    0.000001234 can be safely rounded to 0.00000123 (8 decimal places) -> formatted to 1.23e-06.

    After rounding: if the value is equal or greater than 1000 it gets formatted by intcomma.

    :param float value: Value to be rounded.
    :param no_str: Flag indicating whether number should also be auto-formatted (exponents).
    :return: Rounded value (float) and in case of ``no_str`` flag also formatted value (str).
    :rtype: float or str
    """

    # If value is not instance of a number we don't do anything.
    if not isinstance(value, (int, float)):
        return value

    # Zero is just a zero :)
    if value == 0:
        return 0.0 if no_str else "0"

    # No decimal point -> number is integer.
    if value.is_integer():
        return float(value) if no_str else intcomma(int(value))

    # 1. determine number of numbers before decimal point.
    dec = Decimal(value).as_tuple()
    zeroes_before_decimal = dec.exponent * -1 - len(dec.digits)

    # If there are any zeroes before decimal that means the number is small
    # (perhaps very small). In this case we need to count with these places
    # and also the first non-zero number (that's why we +1 here).
    if dec.exponent * -1 > zeroes_before_decimal > 0:
        decimal_places = zeroes_before_decimal + 1 + settings.MAX_PRECISION
    else:
        decimal_places = settings.MAX_PRECISION

    rounded = round(value, decimal_places)

    if no_str:
        return rounded

    if 1000 <= rounded or -1000 >= rounded:
        return intcomma(rounded)

    return str(rounded)


@register.filter
def to_json(value):
    """
    Converts the given value into json.

    :return: JSON string.
    :rtype: str
    """

    try:
        return json.dumps(value)
    except:
        LOGGER.exception("Couldn't serialize data into JSON.")

        return


@register.filter
def lookup(object, key):
    """
    Looks up for the ``key`` in the given ``object``.
    It doesn't fail (raise and exception) so this filter is chainable.

    :return: Found object (if any) or None.
    :rtype: Any or None
    """

    if object:
        return object.get(key)


@register.simple_tag(takes_context=True)
def wdb(context):
    """
    Spawns web debugger with template context available.
    """

    import wdb

    wdb.set_trace()


@register.filter
def filename(path):
    """
    Returns filename of the given path.

    :return: The filename.
    :rtype: str
    """

    return Path(path).name


@register.filter
def to_quarter_period(date_):
    """
    Converts the given date to quarter string representation in
    format YEAR QX where X is 1 - 4.

    :param date or datetime or od pd.Timestamp or np.datetime64 or np.str_: Date to be converted to period.
    :return: Quarter representation or the input data if fails.
    :rtype: str or any
    """

    # Numpy datetime.
    if np.datetime64 == type(date_):
        date_ = pd.Timestamp(date_)

    # Pandas timestamp.
    if pd.Timestamp == type(date_):
        date_ = date_.to_pydatetime()

    elif type(date_) in (str, np.str_):
        date_ = datetime.strptime(date_, "%Y-%m-%d")

    try:
        return f"{date_.year} Q{int(date_.month / 3)}"

    except:
        LOGGER.exception("Couldn't convert date to quarted period")

        return date_


@register.filter
def sum_by(items, prop):
    """
    Walks thru items and sums up all it's ``prop``
    no matter if it's a property or a callable.

    :param list items: List of items we will go thru.
    :param str prop: Property/callable name.
    :return: Sum of all the properties - in case of error always returns 0.
    :rtype: int or float
    """

    to_return = 0

    try:
        for i in items:
            if callable(getattr(i, prop)):
                to_return += getattr(i, prop)()

            else:
                to_return += getattr(i, prop)
    except:
        return 0

    return to_return
