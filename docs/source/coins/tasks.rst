Tasks
=====

fetch_historical_data(coin=None)
--------------------------------
Fetches historical prices data from the internet (coinmarketcap.com). Uses
`karpet <https://github.com/im-n1/karpet>`__ library which fetches all
possible prices since :doc:`COIN_EPOCH <../architecture/settings>`.

Once it's loaded we sort it by date and start dumping the data into database.
Each price is checked if it's valid (format) and then saved into database.

When all records are saved and there are any new ones we perform following actions:

* regenerate cache (``get_last_days_perc_change()``)
* performance chart
* generate open investments chart

The task takes 1 param. PK of ``Coin``.
If coin PK is specified only that coin is fetched. If not all coins
in the database will be fetched one by one.

fetch_current_price(coin=None)
------------------------------
Fetch current price, market status, market move (price change) both
in value and in percents. All is done by underlying Karpet library.

Price data are also cached.

The task takes 1 param. PK of ``Coin``.
If coin PK is specified only that coin is fetched. If not all coins
in the database will be fetched one by one.
