import logging
import time
from datetime import datetime

import numpy as np
import pytz
from django.core.cache import cache
from django.utils import timezone

from ..celery import app
from ..core.models import Meta, Price
from ..core.scraper import Manager
from ..core.tasks import generate_performance_charts
from ..transactions.transactions import Performance, Transactions
from .models import Index

LOGGER = logging.getLogger("richy.celery")


@app.task
def fetch_historical_data(index=None):
    """
    Fetches new historical data (prices) for the given index.
    If no index was given fetches data for all index.

    If new data has been found we update caches and
    regenerate performance chart.

    :param int share: Index PK.
    """

    def fetch_index(index):
        """
        Fetches index historical data.

        :param Index index: Index model instance.
        """

        LOGGER.debug(f"Downloading prices for {index}.")
        df = Manager.fetch_index_prices(index)

        # Save prices into database.
        for row_index, r in df.iterrows():
            try:
                price = float(r["Close"])
                assert not np.isnan(price)
            except:
                LOGGER.debug(
                    "Price for {} at {} is not valid - it's {}. Skipping ...".format(
                        index.symbol, row_index, r["Close"]
                    )
                )
                continue

            # Create/Update database price record.
            try:
                p, created = Price.objects.get_or_create(
                    item=index,
                    datetime=datetime(
                        row_index.year, row_index.month, row_index.day, tzinfo=pytz.UTC
                    ),
                    defaults={"price": price},
                )

                if created:
                    LOGGER.debug(
                        "Prices successfully saved to database for {} - date {} - ID {}.".format(
                            index.symbol, p.datetime, p.pk
                        )
                    )
            except:
                LOGGER.exception(
                    f"Prices weren't saved to database for {index.symbol} - date {index}!"
                )

    def update_caches(index):
        """
        Delete and/or updates related caches.

        :param Index index: Index model instance.
        """

        index.update_cache()
        cache.delete("index-transaction-performance-and-assets")
        cache.delete("index-transaction-profit-performance")

        Performance("index", profit=False, use_cache=False).get_data()
        Performance("index", profit=True, use_cache=False).get_data()

        # Transactions stats.
        Transactions.update_cache()

    LOGGER.debug("Starting datasets downloader...")

    # Fetch specific share.
    if index:
        index = Index.objects.get(pk=index)
        fetch_index(index)
        update_caches(index)
        generate_performance_charts(index.pk)

    # Or fetch all indexes in the database.
    else:
        for i in Index.objects.order_by("symbol"):
            try:
                fetch_index(i)
            except:
                LOGGER.exception(
                    f"Index historical data fetching has failed.", extra={"index": i}
                )
                LOGGER.debug(f"Index: {i.symbol} ^")
            else:
                update_caches(i)
                generate_performance_charts(i.pk)

    # Save "last price update" meta stamp.
    Meta.objects.update_or_create(
        type=Meta.LAST_PRICE_UPDATE, defaults={"value": timezone.now()}
    )


@app.task(queue="fast")
def fetch_current_price(index=None):
    """
    Fetches current market price and caches
    it under "item-{}-current-price" key for one hour.
    If no coin PK instance is given fetches all indexes in database.
    Sleeps 10 seconds between each fetch.

    :param int Index: Index PK.
    """

    def fetch_index(index):
        price = Manager.get_current_price_and_change(index)

        if price:
            index.set_current_price_and_change(price)

    if index:
        fetch_index(Index.objects.get(pk=index))
    else:
        for i in Index.objects.all():
            try:
                fetch_index(i)
            except:
                LOGGER.exception(
                    "Couldn't fetch index current price", extra={"index": i}
                )
                LOGGER.debug(f"Index: {i} ^")

            time.sleep(10)
