def calc_percentage_change(end, start):
    """
    Calculates percentage difference between
    initial value (start) and final value (end).

    :param int end: Final value.
    :param int start: Initial value.
    :return: Calculated percentage change.
    :rtype: float
    """

    return 100 * (end / start - 1)
