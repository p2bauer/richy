Charts
======
Some charts are rendered on backend (using matplotlib or seaborn
libraries) in ``richy.core.charts`` module. Each chart has it's
own class for clarity sake.

Charts are exported in SVG (by default) - for details there is
``export`` method for each  Matplotlib chart.

.. automethod:: richy.core.charts.BaseChart.export
