Form fields
===========

MultipleFileField
-----------------
File field with multiple upload turned on (user can pick up multiple
files from the disk). Once files are uploaded they can be removed
or user can upload more files.

Initial data is a list of model instances which represents the file in
database. One requirement is that the model **must** have ``file``
property (can be right model property or aliased with method and
``@property`` decorator). File widget then uses the model PK in URL
for deleting the files.

Field extends from ``FileField``.

Special properties are:

* ``delete_url_pattern`` - URL pattern where deletion URL point to -
  must have ``id`` URL parameter

.. figure:: imgs/multiple_file_field.png
   :align: center

   MultipleFileField in action with 2 files and ability to delete
