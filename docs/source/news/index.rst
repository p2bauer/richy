News
====

Pruning
-------
Celery task ``news.tasks.delete_old_records`` prunes all news older than
``settings.NEWS_PRUNE_THRESHOLD_DAYS`` days.
