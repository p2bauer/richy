import ast
import logging

from django import template
from django.utils.safestring import mark_safe

LOGGER = logging.getLogger(__name__)
register = template.Library()


@register.simple_tag
def get_price_status_icon(current_price):
    """
    Returns icon for current market price status.

    - before market is open (pre-market)
    - open
    - after market is closed (post-market)

    .. figure:: imgs/template_tags/get_price_status_icon.png
       :align: center

    :param CurrentPrice current_price: CurrentPrice instance for item or share.
    :return: HTML i tag with the icon.
    :rtype: str
    """

    if not current_price:
        return

    icon = None

    if "pre-market" == current_price.state:
        icon = "hourglass-top"

    elif "open" == current_price.state:
        icon = "hourglass-split"

    elif current_price.state in ("post-market", "closed"):
        icon = "hourglass-bottom"

    if not icon:
        LOGGER.error("Unknown market state")

    return mark_safe(f'<i class="bi bi-{icon} uk-margin-small-right"></i>')


@register.filter
def item_tags_to_classes(tags, prefix):
    return " ".join([f"{prefix}-{t}" for t in tags])


@register.filter
def move_arrow(value, previous_value=None):
    """
    Returns HTML with arrow icon based on the given value. If the value
    is > 0 the arrow is green and trending up. If not it's red and
    trending down.
    """

    if not value:
        return ""

    if isinstance(value, str):
        value = ast.literal_eval(value)

    if isinstance(previous_value, str):
        previous_value = ast.literal_eval(previous_value)

    if previous_value:
        if previous_value < value:
            return mark_safe(
                '<i class="bi bi-arrow-up-right richy-green move-arrow"></i>'
            )

        return mark_safe('<i class="bi bi-arrow-down-right richy-red move-arrow"></i>')
    else:
        if 0 < value:
            return mark_safe(
                '<i class="bi bi-arrow-up-right richy-green move-arrow"></i>'
            )

        return mark_safe('<i class="bi bi-arrow-down-right richy-red move-arrow"></i>')


@register.simple_tag(takes_context=True)
def get_owned_items_sum(context, obj):
    """
    Returns dict with keys "value" and "amount" of current
    shares price and amount.

    Answers question how many items (shares, coins, ...) you currently
    have and what is it's value today.

    :returns: Dict with "amount" and "value" keys.
    """

    return obj.get_owned_items_sum(context.request.user)


@register.simple_tag(takes_context=True)
def get_owned_items(context, obj):
    """
    Returns dict with keys "amount" and "value" of all transactions
    since last closing one (or since the beginning of time).

    Answers question how many items you currently own and
    how much it costed you at the time of buying.

    :returns: Dict with "amount" and "value" keys.
    """

    return obj.get_owned_items(context.request.user)
