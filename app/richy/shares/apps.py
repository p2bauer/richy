from django.apps import AppConfig


class SharesConfig(AppConfig):
    name = "richy.shares"
