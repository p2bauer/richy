Incudables
==========
App comes with a few templates that are used across the app - they
are included into other templates. These templates render common stuff
that are used a lot so they server DRY principe.

All such templates are placed in ``core/templates/`` directory and starts
with ``_`` as prefix.

* ``_fm.pug`` - renders flash messages of all types
* ``_bc.pug`` - renders breadcumb navigation to the current page
* ``_form_field.pug`` - render all kinds of form fields inc. its error
  messages
* ``_menu.pug`` - renders menu in the app layout

    * takes ``title`` param (optional): rewrites default page title

* ``_submenu_dynamic.pug`` - renders submenu under main menu from the
  given items

    * takes ``items`` param: list of submenu items to be rendered

* ``_submenu_sitetree.pug`` - renders submenu based on sitetree items
* ``_title_bc.pug`` - renders page title into H1 and subtitle into H2

    * takes ``title`` param: main title of the page
    * takes ``subtitle`` param: page subtitle written under the main title
    * takes ``small_subtitle`` param: page smaller version of subtitle ^
    * takes ``chip`` param: chip with text next to the headline
