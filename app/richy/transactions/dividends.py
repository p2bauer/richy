import logging
from datetime import date

from ..core.models import User
from .models import DividendTransaction, Transaction
from .transactions import Transactions

LOGGER = logging.getLogger(__name__)


class DividendTransactions:
    """
    This class handles ``DividendTransaction`` model creation/update.
    It calculates based on ``Transaction`` (currently owned stocks) and
    ``shares.Dividend`` (dividend data) transactions that are income
    for share holder.
    """

    def __init__(self, share):
        """
        :param Share share: Share model instance.
        """

        self.share = share

    def calculate(self):
        """
        Calculates (creates/updates) ``DividendTransaction`` model
        records based on owned stocks now and in past.

        Also recalculates/removed already created transaction in the past.
        """

        # Fetch only those users that have at least one transaction.
        for user in User.objects.filter(
            pk__in=Transaction.objects.distinct("user").values_list("user", flat=True)
        ):
            dividends = self.share.dividend_set.filter(
                record_date__lte=date.today()
            ).order_by("record_date")
            df = Transactions(user).get_transaction_basic_stats()

            if df.empty:
                LOGGER.debug(
                    "Empty dataframe (no transactions) - terminating dividend calculation."
                )

                return

            df = df[df["type"] == "share"]

            for div in dividends:
                df_filtered = df[df["date"] <= div.payment_date]
                df_filtered = df_filtered[df_filtered["symbol"] == self.share.symbol]

                # If no shares were held in the time dividend ex date we skip it.
                if df_filtered.empty:
                    continue

                # Calculate currently (div.payment_date) owned shares.
                owned_shares = df_filtered.amount.sum()

                # If some were shared
                if owned_shares:
                    _, created = DividendTransaction.objects.update_or_create(
                        user=user,
                        dividend=div,
                        defaults={
                            "shares": owned_shares,
                            "amount": div.amount_value * owned_shares,
                        },
                    )

                    if created:
                        LOGGER.debug(
                            "Dividend transaction has been calculated for "
                            f"{self.share} for date {div.payment_date} for user with ID {user.pk}."
                        )
                    else:
                        LOGGER.debug(
                            "Dividend transaction has been recalculated for "
                            f"{self.share} for date {div.payment_date} for user with ID {user.pk}."
                        )
                else:
                    try:
                        DividendTransaction.objects.by_user(user).get(
                            dividend=div
                        ).delete()

                        LOGGER.debug(
                            f"Dividend transaction for share {self.share.symbol} "
                            f"and {div.payment_date} for user with ID {user.pk} has been deleted."
                        )
                    except DividendTransaction.DoesNotExist:
                        pass
