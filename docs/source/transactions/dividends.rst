Dividends
=========
``DividendTransaction`` are calculated based on ``shares.Dividend`` and
``Transaction`` models.

calculate()
-----------
.. automethod:: richy.transactions.dividends.DividendTransactions.calculate
