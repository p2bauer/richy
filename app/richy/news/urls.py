from django.urls import path

from . import views

app_name = "news"
urlpatterns = [
    path("", views.DashboardTemplateView.as_view(), name="dashboard"),
    path("fetch/", views.FetchAjaxView.as_view(), name="fetch"),
]
