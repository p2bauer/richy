# Richy

<div align="center">
    <img src="https://gitlab.com/imn1/richy/-/raw/master/assets/screenshots/dashboard.png">
    <p>Personal investing portfolio manager.</p>
</div>

[[_TOC_]]

## Introduction

### What Richy is

- a (passive) portfolio manager
- market news hub
- a tool that aggregates information that helps you form ideas
- much better than your excel sheets
- quite documented

### What Richy is not

- an investing platform like RobinHood
- an app that gives you investing advice
- a trading bot
- a smart app with some kind of AI that tries to predict market

## Instalation

The project is completely in Docker so the easiest way is to use
[prebuilt docker images](https://hub.docker.com/r/n1cz/richy) and
run `docker compose up`. But first you need to provide some basic
config for the app.

Once you provide `.env` file, `docker-compose.yaml` file and
nginx config you are all set up. Here are the details.

### .env file

You need to provide at least the following environment variables:

- BASE_URL - base URL (including protocol) of you app
- DOMAIN - domain you will use to access the app (FQDN)
- TZ - timezone you run the app within

You can use predefined `.env.skel` file which can be found in
the repository root. Copy that to the project root as `.env` and
set the variables inside the file.

### docker compose file
Example compose file `docker-compose.yaml.skel` is in the repository
root and waits to be used to run the app. The compose file contains
all entities required to run the app as a whole + nginx example
(more bellow).

### nginx config
Example nginx config file `nginx.conf.skel` which also sits in the repository
root brings a production-ready config you can use. Just change all the
`your-domain.com` string occurences with your real domain (just like you
did in `.env` file for `DOMAIN` env var) and you are ready to go.

The config is SSL ready - namely brings in Let's Encrypt ready setup.
But you are the one who needs to suply the certs for your own domain.
Of you can change the config to HTTP-only and skip the certificate
providing part.

### user account
The app comes with one predefined user but it takes just one command to
create another one. It can be done in administration (see bellow).

The existing user:

- email (login): `admin@test.com`
- password: `test`

## Administration

As a regular Django application Richy comes with Django Admin. The administration
part sits under `/admin/` URL. You can login with the predefined admin user (see above).

Administration is really meant for a low-level administration like:

- adding a new users
- reseting a user's password
- managing exchanges
- managing periodic tasks
- managing transactions
- managing items (stocks, EFTs, indexes, coins)
- managing stakings
- managing news
- managing site tree (menus)

Use the admin part of the application if you really know what you do.
You can easily ruin your current setup!

## Development

Any help is more than welcomed on this project. Even constructive
criticism. No worries. The goal is to move this project forward and
bring in new ideas. Feel free to clone the repo, run it and create
new merge requests or issues.

### Standards

Project sticks to a few standards so the code is not a mess and to
dodge git conflicts and common pitfalls. Please stick to them

- [Black](https://github.com/psf/black/)
- [Isort](https://github.com/pycqa/isort)
- [Poetry](https://github.com/python-poetry/poetry)
- [Docker](https://github.com/docker)

### Docs
Documentation sits in `/docs` folder. It utilizes Sphinx and can be build
by `$ cd docs ; make html` command.

### Tech stack

#### Backend

The app is backend heavy. All the computations run on backend.

- [Python](https://www.python.org/)
- [Django Framework](https://www.djangoproject.com/)
- [Celery](https://docs.celeryq.dev)
- [Postgres](https://www.postgresql.org/)
- [Redis](https://redis.io/)
- [Graphviz](https://graphviz.org/)
- [Nginx](http://nginx.org/)
- [Gunicorn](https://gunicorn.org/)

#### Frontend

Frontend is very lightweight - basically just an UI framework + a few
Vue.js components for charts.

- [Pug](https://pugjs.org)
- [UIkit](https://getuikit.com/)
- [Vue.js](https://vuejs.org/)
- [Highcharts](https://www.highcharts.com/)
- [SASS](https://sass-lang.com/)
- [Webpack](https://webpack.js.org/)

#### Other

Other tools that help make this project possible are:

- [Sphinx](https://www.sphinx-doc.org)
- [Gitlab CI](https://docs.gitlab.com/ee/ci/)

### Main utilized libraries

There should be a big thank you to all those libraries creators
and its contributors. They are awesome and trully make this
(and tons of others) project possible.

- [Yfinance](https://github.com/ranaroussi/yfinance)
- [Pandas](https://github.com/pandas-dev/pandas/)
- [Numpy](https://github.com/numpy/numpy)
- [Seaborn](https://github.com/mwaskom/seaborn)
- [Matplotlib](https://github.com/matplotlib/matplotlib)

### Co-developed libraries

Those libraries were co-developed with this project as a functionality
that deserves its own place so it can be used this and by the others.
Feel free to try them out.

- [Rug](https://github.com/im-n1/rug) - for stock market data
- [Karpet](https://github.com/im-n1/karpet) - for cryptocurrency market data

## Philosophy

### Technical
The goal is to keep the frontend as slim as possible. All the heavy
lifting is done on backend. The app moves forward slowly but steadily.
The goal is not to jump on every new technology or framework but to
be here as long as possible while be still up to date.

### Investment
Investing is easy if you know what are you doing. Thus no one can tell you what
to do nor an application. This is the reason why you should educate yourself
and not rely on 3rd-parties.
