Coin IDS
========
Coins have symbols just like stocks. But these symbols are not unique
which leads to coin ID. Coin ID is unique coin identifier which identifies
any coin no matter what symbol does it have.

Coin ID's are not needed in most cases. But if you consider for example
token Statera and Starta they both have the same symbol - STA. If system
notices this user will need to enter coin ID too (create/update coin form).
