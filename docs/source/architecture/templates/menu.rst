Menu
====
Menu is separated into 2 parts - main menu and submenu.

Main menu is created via `django-sitetree <https://github.com/idlesign/django-sitetree>`_
library and is managed in Django admin.

Submenu contains following parts (order preserved):

1. pages on the same level as parent of the current page
2. child pages
3. dynamic items

Dynamic submenu is managed programaticaly. If a view needs it's custom
submenu you can create one quite simple thru predefined view
mixin ``richy.core.views.SubmenuViewMixin``. This mixin adds
method ``get_sumenu()`` that you need to implement by yourself.
The method must return list of menu items where each items is a
dict with following properties:

* url - menu item URL
* title - menu item title
* is_current - flag if the menu item is currently active page

Here is simple example that News section uses:

.. literalinclude:: ../../../../../app/app/richy/shares/views.py
   :pyobject: ShareChildrenSubmenuViewMixin.get_submenu

Output then looks like this:

.. figure:: imgs/submenu.png
   :align: center
