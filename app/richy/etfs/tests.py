import logging
from datetime import date, timedelta
from unittest.mock import patch

import numpy as np
from django.urls import reverse
from django.utils import timezone

from ..core.math import calc_percentage_change
from ..core.models import ItemData, Price, UserItem
from ..core.tests import (
    BaseCacheTestCase,
    BaseDeleteUserItemTestCase,
    BaseItemOverviewAllocationTestCase,
    BaseTestCase,
    create_items,
    create_transactions,
    create_user_items,
)
from ..transactions.models import Transaction
from .models import Etf
from .tasks import (
    fetch_basic_info,
    fetch_current_price,
    fetch_historical_data,
    fetch_holdings,
)

LOGGER = logging.getLogger(__name__)


class HistoricalDataTestCase(BaseTestCase):
    def test_fetch_historical_data_without_transactions(self):
        """
        Tests fetching historical data (prices) of a share
        without any transactions in the database.
        """

        # Let's operate with TSLA ticket which should
        # have more than 1000 historical records.
        s = Etf(symbol="SPLG")
        s.save()

        fetch_historical_data(s.pk)

        count = Price.objects.count()
        LOGGER.debug(f"Downloaded {count} historical records.")

        # Let's consider more than 1000 results as success.
        self.assertTrue(1000 < count)

    def test_fetch_historical_data_with_transactions(self):
        """
        Tests fetching historical data (prices) of a share
        with some transactions in the database.

        This test also indirectly tests following methods as long
        as they are called from ``fetch_historical_data()`` task.

        * update_caches()
        * generate_performance_charts()
        """

        # Let's operate with TSLA ticket which should
        # have more than 1000 historical records.
        s = Etf(symbol="SPLG")
        s.save()

        # Also create a transaction.
        t = Transaction(
            item=s,
            price=250,  # pure guess
            amount=10,
            fee=10,
            date=date.today() - timedelta(days=300),
            is_deposit=True,
            user=self.user,
        )
        t.save()

        fetch_historical_data(s.pk)

        count = Price.objects.count()
        LOGGER.debug(f"Downloaded {count} historical records.")

        # Let's consider more than 1000 results as success.
        self.assertTrue(1000 < count)


class PercentageChangesTestCase(BaseTestCase):
    def test_compounding_change(self):
        """
        Tests compounded change for SPLG with 4 investments where
        each day price rose.
        """

        create_items()
        splg = Etf.objects.get(symbol="SPLG")
        create_transactions(
            [
                [
                    {
                        "item": splg,
                        "price": 10,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": splg,
                        "price": 11,
                        "amount": 500,
                        "fee": 0,
                        "date": "2018-12-02",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": splg,
                        "price": 12,
                        "amount": 750,
                        "fee": 0,
                        "date": "2018-12-03",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": splg,
                        "price": 13,
                        "amount": 250,
                        "fee": 0,
                        "date": "2018-12-04",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ],
        )

        # Create price for item.
        Price.objects.create(item=splg, datetime="2018-12-01", price=10)
        Price.objects.create(item=splg, datetime="2018-12-02", price=11)
        Price.objects.create(item=splg, datetime="2018-12-03", price=12)
        Price.objects.create(item=splg, datetime="2018-12-04", price=13)
        Price.objects.create(item=splg, datetime="2018-12-05", price=14)
        Price.objects.create(item=splg, datetime="2018-12-06", price=15)

        # Calculate price change for last day.
        change = (
            Price.objects.order_by("-datetime")[0].price
            - Price.objects.order_by("-datetime")[1].price
        )

        # Calculate investment for each transactions.
        investments = []
        investment_perc_changes = []

        for trans in Transaction.objects.all():
            investment = trans.amount * trans.price
            investments.append(investment)
            investment_perc_changes.append(
                calc_percentage_change(
                    trans.amount * (trans.price + change), investment
                )
            )

        self.assertEqual(
            np.average(investment_perc_changes, weights=investments),
            splg.get_last_days_perc_change(1, no_cache=True, compounded=True),
        )


class BasicInfoTestCase(BaseCacheTestCase):
    def test_fetch_basic_info_all(self):
        create_items()
        fetch_basic_info()

        self.assertEqual(2, ItemData.objects.count())
        self.assertEqual(
            sorted(list(ItemData.objects.first().data["basic_info"].keys())),
            sorted(
                [
                    "asset_class",
                    "asset_class_size",
                    "assets_under_management",
                    "brand",
                    "category",
                    "description",
                    "expense_ratio",
                    "inception",
                    "index_tracked",
                    "issuer",
                    "region_(general)",
                    "region_(specific)",
                    "structure",
                    "year_high",
                    "year_low",
                ]
            ),
        )

    def test_fetch_basic_info(self):
        create_items()
        fetch_basic_info(Etf.objects.first().pk)

        self.assertEqual(1, ItemData.objects.count())
        self.assertEqual(
            sorted(list(ItemData.objects.first().data["basic_info"].keys())),
            sorted(
                [
                    "asset_class",
                    "asset_class_size",
                    "assets_under_management",
                    "brand",
                    "category",
                    "description",
                    "expense_ratio",
                    "inception",
                    "index_tracked",
                    "issuer",
                    "region_(general)",
                    "region_(specific)",
                    "structure",
                    "year_high",
                    "year_low",
                ]
            ),
        )

    def test_detail_page_no_transactions(self):
        create_items()
        create_user_items()
        item = UserItem.objects.filter(item__etf__isnull=False).first()
        fetch_basic_info(item.item.pk)

        response = self.c.get(reverse("etfs:etf_detail", args=[item.pk]))

        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Market cap", response.content)
        self.assertIn(b"Description", response.content)
        self.assertIn(b"Year low - high", response.content)
        self.assertNotIn(b"Current wealth", response.content)

    def test_detail_page_one_transaction(self):
        create_items()
        create_user_items()
        item = UserItem.objects.filter(item__etf__isnull=False).first()
        fetch_basic_info(item.item.pk)
        create_transactions(
            [
                [
                    {
                        "item": item.item,
                        "price": 10,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ]
        )

        # Create price for item.
        Price.objects.create(item=item.item, datetime="2018-12-01", price=10)
        Price.objects.create(item=item.item, datetime="2018-12-02", price=11)

        response = self.c.get(reverse("etfs:etf_detail", args=[item.pk]))

        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Market cap", response.content)
        self.assertIn(b"Description", response.content)
        self.assertIn(b"Current wealth", response.content)
        self.assertIn(
            b'<span title="Investment">10,000</span>&nbsp; -> &nbsp;<span title="Market value">11,000</span>&nbsp;<span title="Amount">(1,000)</span>',
            response.content,
        )

    def test_detail_page_multiple_transactions(self):
        create_items()
        create_user_items()
        item = UserItem.objects.filter(item__etf__isnull=False).first()
        fetch_basic_info(item.item.pk)
        create_transactions(
            [
                [
                    {
                        "item": item.item,
                        "price": 10,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": item.item,
                        "price": 10,
                        "amount": 1000,
                        "fee": 0,
                        "date": "2018-12-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ]
        )

        # Create price for item.
        Price.objects.create(item=item.item, datetime="2018-12-01", price=10)
        Price.objects.create(item=item.item, datetime="2018-12-02", price=11)

        response = self.c.get(reverse("etfs:etf_detail", args=[item.pk]))

        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Market cap", response.content)
        self.assertIn(b"Description", response.content)
        self.assertIn(b"Current wealth", response.content)
        self.assertIn(
            b'<span title="Investment">20,000</span>&nbsp; -> &nbsp;<span title="Market value">22,000</span>&nbsp;<span title="Amount">(2,000)</span>',
            response.content,
        )


class HoldingsTestCase(BaseTestCase):
    def test_fetch_holdings(self):
        create_items()
        fetch_holdings()

        self.assertEqual(2, ItemData.objects.count())
        self.assertEqual(
            len(ItemData.objects.get(item__symbol="SPLG").data["holdings"]), 15
        )
        self.assertEqual(
            len(ItemData.objects.get(item__symbol="SPLG").data["holdings"][0]), 3
        )


class CurrentPriceTestCase(BaseCacheTestCase):
    @patch("rug.yahoo.Yahoo.get_current_price_change")
    def test_share(self, mock):
        """
        Tests share current price, market state,  price change in
        percents and value caching.
        """

        # SPLG data for 28.12.2022
        mock.return_value = {
            "state": "pre-market",
            "pre_market": {
                "change": {"percents": 0.30887648, "value": 0.13850021},
                "value": 44.9785,
            },
            "current_market": {
                "change": {"percents": -0.4219379, "value": -0.18999863},
                "value": 44.84,
            },
            "post_market": {
                "change": {"percents": -0.08920810700000001, "value": -0.0400009},
                "value": 44.8,
            },
        }
        create_items()

        etf = Etf.objects.get(symbol="SPLG")
        fetch_current_price(etf.pk)
        price = etf.get_last_price(current=True)

        self.assertEqual(price.price, mock.return_value["pre_market"]["value"])
        self.assertEqual(price.state, "pre-market")
        self.assertEqual(
            price.change_value, mock.return_value["pre_market"]["change"]["value"]
        )
        self.assertEqual(
            price.change_percents,
            mock.return_value["pre_market"]["change"]["percents"],
        )


class DeleteTestCase(BaseDeleteUserItemTestCase):
    model = Etf
    url = "etfs:delete_etf"
    target_url = "etfs:overview"
    symbol = "TQQQ"
    task = fetch_historical_data

    def test(self):
        self.spawn()


class OverviewAllocationViewTestCase(BaseItemOverviewAllocationTestCase):
    def test_allocation(self):
        self.prepare(
            [
                [str(timezone.now().date()), "SPLG", 2],
                # [str(timezone.now().date()), "TQQQ", 4],
            ]
        )

        splg = Etf.objects.get(symbol="SPLG")
        # tqqq = Etf.objects.get(symbol="TQQQ")

        self.spawn(
            [
                [
                    {
                        "item": splg,
                        "price": 2,
                        "amount": 8,
                        "fee": 0,
                        "date": "2023-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                # [
                #     {
                #         "item": tqqq,
                #         "price": 4,
                #         "amount": 10,
                #         "fee": 0,
                #         "date": "2023-01-01",
                #         "is_deposit": True,
                #         "user": self.user,
                #     },
                #     {},
                # ],
            ],
            "etfs:overview",
        )
