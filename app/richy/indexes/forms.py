from ..core.forms import BaseUserItemForm
from .models import Index


class UserIndexForm(BaseUserItemForm):
    item_model = Index
