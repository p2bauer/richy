import logging
import math
from datetime import datetime, timedelta
from unittest.mock import patch

import numpy as np
import pandas as pd
import pytz
from bs4 import BeautifulSoup
from django.conf import settings
from django.test import Client, SimpleTestCase, TransactionTestCase, override_settings
from django.urls import reverse
from django.utils.timezone import make_aware
from faker import Faker

from ..coins.models import Coin
from ..etfs.models import Etf
from ..indexes.models import Index
from ..shares.models import Share
from ..transactions.models import Exchange, Transaction
from ..transactions.transactions import Transactions
from ..transactions.views import TransactionChartDataMixin
from .charts import DashboardMarketValueRatioPieChart, TransactionOverviewChart
from .models import Item, Price, User, UserItem, date_to_highcharts_timestamp
from .templatetags.utils import autofloatformat, to_quarter_period

LOGGER = logging.getLogger(__name__)


def create_items():
    """
    Creates some basic symbols.
    """

    Coin(symbol="BTC", coin_id="bitcoin").save()
    Coin(symbol="ETH", coin_id="ethereum").save()
    Coin(symbol="ETC", coin_id="ethereum-classic").save()
    Coin(symbol="TRX", coin_id="tron").save()
    Coin(symbol="XRP", coin_id="ripple").save()
    Coin(symbol="LTC", coin_id="litecoin").save()
    Share(symbol="AMD").save()
    Share(symbol="QCOM").save()
    Share(symbol="MSFT").save()
    Share(symbol="TSLA").save()
    Index(symbol="SOX").save()
    Index(symbol="GSPC").save()
    Etf(symbol="SPLG").save()
    Etf(symbol="TQQQ").save()


def create_user_items(user=None):
    if not user:
        user = User.objects.get(email=UserTestMixin.USER_EMAIL)

    for c in Coin.objects.all():
        UserItem(user=user, item=c).save()

    for s in Share.objects.all():
        UserItem(user=user, item=s).save()

    for i in Index.objects.all():
        UserItem(user=user, item=i).save()

    for e in Etf.objects.all():
        UserItem(user=user, item=e).save()


def create_exchanges():
    """
    Creates some basic exchanges.
    """

    Exchange(title="Binance").save()
    Exchange(title="Coinbase").save()
    Exchange(title="Kraken").save()


def create_transactions(transactions):
    """
    Creates transactions according to the given schema.

    :param list transactions: Transactions schema.
    """

    for data, relations in transactions:
        trans = Transaction(**data)
        trans.save()

        if "parents" in relations:
            trans.parents.set(relations["parents"])


def create_prices(prices):
    """
    Creates price records according to given schema.
    The given date is treaten as UTC.

    Prices has following schema:

    ```
    [
        ["2018-01-01", "BTC", 1],
        ["2018-01-02", "BTC", 1.25],
        ....
    ]
    ```

    :param list prices: Prices schema.
    """

    for date, symbol, price in prices:
        try:
            item = Coin.objects.get(symbol=symbol)
        except Coin.DoesNotExist:
            try:
                item = Share.objects.get(symbol=symbol)
            except:
                item = Etf.objects.get(symbol=symbol)

        date = make_aware(datetime.strptime(date, "%Y-%m-%d"), pytz.UTC)
        Price.objects.create(item=item, datetime=date, price=price)


class UserTestMixin:
    USER_EMAIL = "test@test.cz"
    USER_PASSWORD = "test"

    @classmethod
    def set_up_users(cls):
        cls.user = User.objects.create_user(
            UserTestMixin.USER_EMAIL, UserTestMixin.USER_PASSWORD
        )
        cls.user2 = User.objects.create_user(
            "tes2t@test.com", UserTestMixin.USER_PASSWORD
        )


@override_settings(
    CACHES={
        "default": {
            "BACKEND": "django.core.cache.backends.dummy.DummyCache",
        }
    }
)
class BaseTestCase(UserTestMixin, TransactionTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.f = Faker()

    @classmethod
    def setUp(cls):
        super().setUp(cls)
        cls.set_up_users()

        # Create an user and sign him in.
        cls.c = Client()
        cls.c.force_login(cls.user)


@override_settings(
    CACHES={
        "default": {
            "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        }
    }
)
class BaseCacheTestCase(BaseTestCase):
    """
    Base test case with cache support.
    """


class DashboardTestCase(BaseTestCase):
    fixtures = ["sitetree", "meta"]

    def test_dashboard(self):
        response = self.c.get(reverse("dashboard"))

        self.assertIn("<h1>Dashboard", response.content.decode("utf-8"))


class ChartTestCase(BaseTestCase):
    reset_sequences = True

    def fake_market_prices(self):
        """
        Fakes market prices like:

                      BTC    XRP    ETH    LTC
        2018-12-01      1      1    0.5    0.25
        2018-12-02      1      1    0.5    0.25
        2018-12-03      1   0.25    0.5    0.25
        2018-12-04      1   0.25    0.5    0.25
        2018-12-05      1   0.25    0.5    0.25
        2018-12-06      1   0.25      1    0.25
        2018-12-07      2    0.5      1    0.25
        """

        btc = Item.objects.get(symbol="BTC")
        xrp = Item.objects.get(symbol="XRP")
        eth = Item.objects.get(symbol="ETH")
        ltc = Item.objects.get(symbol="LTC")

        Price(item=btc, datetime="2018-12-01", price=1).save()
        Price(item=btc, datetime="2018-12-02", price=1).save()
        Price(item=btc, datetime="2018-12-03", price=1).save()
        Price(item=btc, datetime="2018-12-04", price=1).save()
        Price(item=btc, datetime="2018-12-05", price=1).save()
        Price(item=btc, datetime="2018-12-06", price=1).save()
        Price(item=btc, datetime="2018-12-07", price=2).save()

        Price(item=xrp, datetime="2018-12-01", price=1).save()
        Price(item=xrp, datetime="2018-12-02", price=1).save()
        Price(item=xrp, datetime="2018-12-03", price=0.25).save()
        Price(item=xrp, datetime="2018-12-04", price=0.25).save()
        Price(item=xrp, datetime="2018-12-05", price=0.25).save()
        Price(item=xrp, datetime="2018-12-06", price=0.25).save()
        Price(item=xrp, datetime="2018-12-07", price=0.5).save()

        Price(item=eth, datetime="2018-12-01", price=0.5).save()
        Price(item=eth, datetime="2018-12-02", price=0.5).save()
        Price(item=eth, datetime="2018-12-03", price=0.5).save()
        Price(item=eth, datetime="2018-12-04", price=0.5).save()
        Price(item=eth, datetime="2018-12-05", price=0.5).save()
        Price(item=eth, datetime="2018-12-06", price=1).save()
        Price(item=eth, datetime="2018-12-07", price=1).save()

        Price(item=ltc, datetime="2018-12-01", price=0.25).save()
        Price(item=ltc, datetime="2018-12-02", price=0.25).save()
        Price(item=ltc, datetime="2018-12-03", price=0.25).save()
        Price(item=ltc, datetime="2018-12-04", price=0.25).save()
        Price(item=ltc, datetime="2018-12-05", price=0.25).save()
        Price(item=ltc, datetime="2018-12-06", price=0.25).save()
        Price(item=ltc, datetime="2018-12-07", price=0.25).save()

    def test_transaction_overview(self):
        """
        Tests transaction overview chart series.

              -                                         -
         _   | |       -    -             -        -   |-|   -
        | |  | |      |-|  | |       -   |-|      |-|  |-|  |-|
        | |  | |      | |  | |      | |  | |      | |  | |  | |

        Schema:
        (Binance)

        1)

        $50 -> [1]BTC(+50/1)

        2)

        $50 -> [2]XRP(+200/0.25) -> [3]XRP(-200/0.25) -
                                                        \
                                                          -> [6]ETH(+200/0.5)
                                                        /
        $50 -> [4]ETH(+100/0.5) -> [5]ETH(-100/0.5) ---

        (Kraken)
        3)

        $100 -> [7]ETH(+200/0.5) -
                                   \
                                     -> [8]ETH(-100/1) -> [9]LTC(+200/0.25)

        (Coinbase)
        4)

        $50 -> [10]BTC(+50/1) -> [11]BTC(-50/1) -      -> [14](LTC)[200/0.25] -> [17](LTC)[-100/0.25]
                                                  \  /
                                                    .  -> [15](XRP)[100/0.25] -> [18](XRP)[-30/0.5]
                                                  /  \
        $50 -> [12]BTC(+50/1) -> [13]XRP(-50/1) -      -> [16](ETH)[100/0.25]
        """

        self.maxDiff = None
        create_exchanges()
        create_items()
        # 1
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-01",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ]
            ]
        )
        # 2)
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": -200,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [2]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 100,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": -100,
                        "fee": 0,
                        "date": "2018-12-03",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [4]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.5,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-06",
                        "exchange": Exchange.objects.get(title="Binance"),
                        "user": self.user,
                    },
                    {"parents": [3, 5]},
                ],
            ]
        )
        # 3
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.5,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 1,
                        "amount": -100,
                        "fee": 0,
                        "date": "2018-12-06",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {"parents": [7]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="LTC"),
                        "price": 0.25,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Kraken"),
                        "user": self.user,
                    },
                    {"parents": [8]},
                ],
            ]
        )
        # 4
        create_transactions(
            [
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -50,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {"parents": [10]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": 50,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1,
                        "amount": -50,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {"parents": [12]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="LTC"),
                        "price": 0.25,
                        "amount": 200,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {"parents": [11, 13]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.25,
                        "amount": 100,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {"parents": [11, 13]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 0.25,
                        "amount": 100,
                        "fee": 0,
                        "date": "2018-12-05",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "user": self.user,
                    },
                    {"parents": [11, 13]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="LTC"),
                        "price": 0.25,
                        "amount": -100,
                        "fee": 0,
                        "date": "2018-12-06",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [14]},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="XRP"),
                        "price": 0.5,
                        "amount": -30,
                        "fee": 0,
                        "date": "2018-12-07",
                        "exchange": Exchange.objects.get(title="Coinbase"),
                        "is_closing": True,
                        "user": self.user,
                    },
                    {"parents": [15]},
                ],
            ]
        )
        self.fake_market_prices()

        # First test chain groups (pile).
        df = Transactions(self.user).get_transaction_basic_stats(closed=False)
        groups = Transactions(self.user).get_pile_stats(df)
        self.assertEqual(
            groups,
            [
                (
                    [1],
                    {
                        "open_symbols": [
                            {"symbol": "BTC", "amount": 50.0, "market_price": 100.0}
                        ],
                        "investments": [{1: 50.0}],
                        "incomes": [],
                        "fees": 0.0,
                    },
                ),
                (
                    [10.0, 12.0],
                    {
                        "open_symbols": [
                            {"symbol": "BTC", "amount": 0.0, "market_price": 0.0},
                            {"symbol": "ETH", "amount": 100.0, "market_price": 100.0},
                            {"symbol": "LTC", "amount": 100.0, "market_price": 25.0},
                            {"symbol": "XRP", "amount": 70.0, "market_price": 35.0},
                        ],
                        "investments": [{10: 50.0}, {12: 50.0}],
                        "incomes": [{17: 25.0}, {18: 15.0}],
                        "fees": 0.0,
                    },
                ),
                (
                    [2, 4.0],
                    {
                        "open_symbols": [
                            {"symbol": "ETH", "amount": 0.0, "market_price": 0.0},
                            {"symbol": "XRP", "amount": 200.0, "market_price": 100.0},
                        ],
                        "investments": [{2: 50.0}, {4: 50.0}],
                        "incomes": [],
                        "fees": 0.0,
                    },
                ),
                (
                    [7],
                    {
                        "open_symbols": [
                            {"symbol": "ETH", "amount": 100.0, "market_price": 100.0},
                            {"symbol": "LTC", "amount": 200.0, "market_price": 50.0},
                        ],
                        "investments": [{7: 100.0}],
                        "incomes": [],
                        "fees": 0.0,
                    },
                ),
            ],
        )

        # Second test chart data.
        series = TransactionOverviewChart(self.user, df).get_series()
        self.assertEqual(
            series,
            [
                {
                    "name": "Investments",
                    "data": [50.0, 50.0, 50.0, 100.0],
                    "stack": "investments",
                },
                {
                    "name": "Investments",
                    "data": [0.0, 50.0, 50.0, 0.0],
                    "stack": "investments",
                },
                {
                    "name": "Market values",
                    "data": [
                        {"name": "BTC", "y": 100.0},
                        {"name": "BTC", "y": 0.0},
                        {"name": "ETH", "y": 0.0},
                        {"name": "ETH", "y": 100.0},
                    ],
                    "stack": "market_value",
                },
                {
                    "name": "Market values",
                    "data": [
                        {"name": np.NaN, "y": 0.0},
                        {"name": "ETH", "y": 100.0},
                        {"name": "XRP", "y": 100.0},
                        {"name": "LTC", "y": 50.0},
                    ],
                    "stack": "market_value",
                },
                {
                    "name": "Market values",
                    "data": [
                        {"name": np.NaN, "y": 0.0},
                        {"name": "LTC", "y": 25.0},
                        {"name": np.NaN, "y": 0.0},
                        {"name": np.NaN, "y": 0.0},
                    ],
                    "stack": "market_value",
                },
                {
                    "name": "Market values",
                    "data": [
                        {"name": np.NaN, "y": 0.0},
                        {"name": "XRP", "y": 35.0},
                        {"name": np.NaN, "y": 0.0},
                        {"name": np.NaN, "y": 0.0},
                    ],
                    "stack": "market_value",
                },
                {"name": "Sold", "data": [0.0, 25.0, 0.0, 0.0], "stack": "sold"},
                {"name": "Sold", "data": [0.0, 15.0, 0.0, 0.0], "stack": "sold"},
            ],
        )


class DashboardMarketValueRatioChartTestCase(BaseTestCase):
    reset_sequences = True

    class Request:
        def __init__(self, user):
            self.user = user

    def test_single_share(self):
        """
        Tests dashboard net worth chart with single share.

        MSFT
        ----

        Prices:
        2018-01-01 -> 100
        2018-01-02 -> 105
        2018-01-03 -> 110
        2018-01-04 -> 110
        2018-01-05 -> 120

        Investment: $1000
        Profit: 20%
        Market value: $1200
        """

        create_items()
        create_user_items()
        create_prices(
            [
                ["2018-01-01", "MSFT", 100],
                ["2018-01-02", "MSFT", 105],
                ["2018-01-03", "MSFT", 110],
                ["2018-01-04", "MSFT", 110],
                ["2018-01-05", "MSFT", 120],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": Share.objects.get(symbol="MSFT"),
                        "price": 100,
                        "amount": 10,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ]
        )

        mixin = TransactionChartDataMixin()
        mixin.request = self.__class__.Request(self.user)
        self.assertEqual(
            DashboardMarketValueRatioPieChart().get_series(
                mixin.get_transaction_basic_chart_data()
            ),
            {"USD": [{"name": "Net worth", "data": [{"name": "Share", "y": 1200.0}]}]},
        )

    def test_multiple_shares(self):
        """
        Tests dashboard net worth chart with multiple shares.

        MSFT                        AMD
        ----                        ---

        Prices:                     Prices:
        2018-01-01 -> 100           2018-01-01 -> 50
        2018-01-02 -> 105           2018-01-02 -> 50
        2018-01-03 -> 110           2018-01-03 -> 50
        2018-01-04 -> 110           2018-01-04 -> 50
        2018-01-05 -> 120           2018-01-05 -> 40

        Investment: $1000           Investment: $500
        Profit: 20%                 Profit: -20%
        Market value: $1200         Market value: $400
        """

        create_items()
        create_prices(
            [
                ["2018-01-01", "MSFT", 100],
                ["2018-01-02", "MSFT", 105],
                ["2018-01-03", "MSFT", 110],
                ["2018-01-04", "MSFT", 110],
                ["2018-01-05", "MSFT", 120],
                ["2018-01-01", "AMD", 50],
                ["2018-01-02", "AMD", 50],
                ["2018-01-03", "AMD", 50],
                ["2018-01-04", "AMD", 50],
                ["2018-01-05", "AMD", 40],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": Share.objects.get(symbol="MSFT"),
                        "price": 100,
                        "amount": 10,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Share.objects.get(symbol="AMD"),
                        "price": 50,
                        "amount": 10,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ]
        )

        mixin = TransactionChartDataMixin()
        mixin.request = self.__class__.Request(self.user)
        self.assertEqual(
            DashboardMarketValueRatioPieChart().get_series(
                mixin.get_transaction_basic_chart_data()
            ),
            {"USD": [{"name": "Net worth", "data": [{"name": "Share", "y": 1600.0}]}]},
        )

    def test_multiple_shares_and_multiple_coins(self):
        """
        Tests dashboard net worth chart with multiple shares.

        MSFT                        AMD
        ----                        ---

        Prices:                     Prices:
        2018-01-01 -> 100           2018-01-01 -> 50
        2018-01-02 -> 105           2018-01-02 -> 50
        2018-01-03 -> 110           2018-01-03 -> 50
        2018-01-04 -> 110           2018-01-04 -> 50
        2018-01-05 -> 120           2018-01-05 -> 40

        Investment: $1000           Investment: $500
        Profit: 20%                 Profit: -20%
        Market value: $1200         Market value: $400

        BTC                         ETH
        ---                         ---

        Prices:                     Prices:
        2018-01-01 -> 1000          2018-01-01 -> 200
        2018-01-02 -> 1000          2018-01-02 -> 200
        2018-01-03 -> 1100          2018-01-03 -> 200
        2018-01-04 -> 1100          2018-01-04 -> 200
        2018-01-05 -> 1200          2018-01-05 -> 170

        Investment: $1000           Investment: $500
        Profit: 20%                 Profit: -15%
        Market value: $1200         Market value: $425
        """

        create_items()
        create_prices(
            [
                ["2018-01-01", "MSFT", 100],
                ["2018-01-02", "MSFT", 105],
                ["2018-01-03", "MSFT", 110],
                ["2018-01-04", "MSFT", 110],
                ["2018-01-05", "MSFT", 120],
                ["2018-01-01", "AMD", 50],
                ["2018-01-02", "AMD", 50],
                ["2018-01-03", "AMD", 50],
                ["2018-01-04", "AMD", 50],
                ["2018-01-05", "AMD", 40],
                ["2018-01-01", "BTC", 1000],
                ["2018-01-02", "BTC", 1000],
                ["2018-01-03", "BTC", 1100],
                ["2018-01-04", "BTC", 1100],
                ["2018-01-05", "BTC", 1200],
                ["2018-01-01", "ETH", 200],
                ["2018-01-02", "ETH", 200],
                ["2018-01-03", "ETH", 200],
                ["2018-01-04", "ETH", 200],
                ["2018-01-05", "ETH", 170],
            ]
        )
        create_transactions(
            [
                [
                    {
                        "item": Share.objects.get(symbol="MSFT"),
                        "price": 100,
                        "amount": 10,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Share.objects.get(symbol="AMD"),
                        "price": 50,
                        "amount": 10,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="BTC"),
                        "price": 1000,
                        "amount": 1,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
                [
                    {
                        "item": Coin.objects.get(symbol="ETH"),
                        "price": 200,
                        "amount": 2.5,
                        "fee": 0,
                        "date": "2018-01-01",
                        "is_deposit": True,
                        "user": self.user,
                    },
                    {},
                ],
            ]
        )

        mixin = TransactionChartDataMixin()
        mixin.request = self.__class__.Request(self.user)
        data = DashboardMarketValueRatioPieChart().get_series(
            mixin.get_transaction_basic_chart_data()
        )

        LOGGER.debug(data)

        coin = list(filter(lambda i: "Coin" == i["name"], data["USD"][0]["data"]))
        share = list(filter(lambda i: "Share" == i["name"], data["USD"][0]["data"]))

        self.assertEqual(coin, [{"name": "Coin", "y": 1625.0}])
        self.assertEqual(share, [{"name": "Share", "y": 1600.0}])


class TemplateTagsTestCase(SimpleTestCase):
    def test_autofloatformat(self):
        # Rounding.
        self.assertEqual(autofloatformat(1234.0, no_str=True), 1234.0)
        self.assertEqual(autofloatformat(0.0, no_str=True), 0.0)
        self.assertEqual(autofloatformat(1234.1234, no_str=True), 1234.123)
        self.assertEqual(autofloatformat(1234.0, no_str=True), 1234)
        self.assertEqual(autofloatformat(0.001234, no_str=True), 0.001234)
        self.assertEqual(autofloatformat(0.00001234, no_str=True), 0.00001234)

        # Rounding + formatting.
        self.assertEqual(autofloatformat(1234.0), "1,234")
        self.assertEqual(autofloatformat(0.0), "0")
        self.assertEqual(autofloatformat(1234.1234), "1,234.123")
        self.assertEqual(autofloatformat(1234.0), "1,234")
        self.assertEqual(autofloatformat(0.001234), "0.001234")
        self.assertEqual(autofloatformat(0.00001234), "1.234e-05")

    def test_to_quarter_period(self):
        self.assertEqual(
            to_quarter_period(np.datetime64(datetime(2022, 8, 13, 0, 0, 0))), "2022 Q2"
        )
        self.assertEqual(
            to_quarter_period(pd.Timestamp(datetime(2022, 8, 13, 0, 0, 0))), "2022 Q2"
        )
        self.assertEqual(to_quarter_period("2022-08-13"), "2022 Q2")
        self.assertEqual(to_quarter_period(np.str_("2022-08-13")), "2022 Q2")
        self.assertEqual(to_quarter_period(123), 123)


@override_settings(
    CACHES={
        "default": {
            "BACKEND": "django.core.cache.backends.dummy.DummyCache",
        }
    }
)
class TheEyeShareIndexEtfTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

        # Must be (timezone) aware.
        self.now = None
        self.df = None
        self.open_df = None
        create_items()
        create_user_items()

    @patch("yfinance.Ticker.history")
    def test_fetch_view_pre_market(self, mocked_history):
        mocked_history.side_effect = self.generate_market_data
        self.now = datetime(
            2022, 1, 1, 8, 0, 0, tzinfo=pytz.timezone("America/New_York")
        )
        item = self.get_item()

        response = self.c.get(reverse("core:the_eye_fetch", args=[item.symbol]))
        response = response.json()

        self.assertTrue(response["status"])
        self.assertEqual(response["data"]["symbol"], item.symbol.upper())
        self.assertIsNone(response["data"]["open"])
        self.assertIsNone(response["data"]["close"])
        self.assertEqual(
            response["data"]["min"], autofloatformat(self.df["Low"].min(), no_str=True)
        )
        self.assertEqual(
            response["data"]["max"], autofloatformat(self.df["High"].max(), no_str=True)
        )
        self.assertEqual(len(response["data"]["prices"]), len(self.df))

    @patch("yfinance.Ticker.history")
    def test_fetch_view_open_market(self, mocked_history):
        mocked_history.side_effect = self.generate_market_data
        self.now = datetime(
            2022, 1, 1, 10, 0, 0, tzinfo=pytz.timezone("America/New_York")
        )
        item = self.get_item()

        response = self.c.get(reverse("core:the_eye_fetch", args=[item.symbol]))
        response = response.json()

        self.assertTrue(response["status"])
        self.assertEqual(response["data"]["symbol"], item.symbol.upper())
        self.assertEqual(
            response["data"]["open"],
            date_to_highcharts_timestamp(
                self.open_df.index[0].tz_convert(settings.TIME_ZONE)
            ),
        )
        self.assertIsNone(response["data"]["close"])
        self.assertEqual(
            response["data"]["min"], autofloatformat(self.df["Low"].min(), no_str=True)
        )
        self.assertEqual(
            response["data"]["max"], autofloatformat(self.df["High"].max(), no_str=True)
        )
        self.assertEqual(len(response["data"]["prices"]), len(self.df))

    @patch("yfinance.Ticker.history")
    def test_fetch_view_post_market(self, mocked_history):
        mocked_history.side_effect = self.generate_market_data
        self.now = datetime(
            2022, 1, 1, 17, 0, 0, tzinfo=pytz.timezone("America/New_York")
        )
        item = self.get_item()

        response = self.c.get(reverse("core:the_eye_fetch", args=[item.symbol]))
        response = response.json()

        self.assertTrue(response["status"])
        self.assertEqual(response["data"]["symbol"], item.symbol.upper())
        self.assertEqual(
            response["data"]["open"],
            date_to_highcharts_timestamp(
                self.open_df.index[0].tz_convert(settings.TIME_ZONE)
            ),
        )
        self.assertEqual(
            response["data"]["close"],
            date_to_highcharts_timestamp(
                self.open_df.index[-1].tz_convert(settings.TIME_ZONE)
            ),
        )
        self.assertEqual(
            response["data"]["min"], autofloatformat(self.df["Low"].min(), no_str=True)
        )
        self.assertEqual(
            response["data"]["max"], autofloatformat(self.df["High"].max(), no_str=True)
        )
        self.assertEqual(len(response["data"]["prices"]), len(self.df))

    def generate_market_data(self, *args, **kwargs):
        """
        Creates dataframe with current (fake) market data.
        Is directed by ``self.now`` and ``prepost`` param
        in ``kwargs``.

        Emulates yfinance.Ticker.history() method.

        Generated dataframe has following columns:

        - Date (index)
        - Open
        - High
        - Low
        - Close
        - Volume
        - Dividends
        - Stock Splits
        """

        # NYSE opening hours
        # - pre-market starts at 4:00am EST
        # - stock exchange opens at 9:30am EST
        # - stock exchange closes at 4:00pm EST
        # - post-market ends at 7pm EST

        # Are made aware later when index is created.
        pre_period_start = datetime(2022, 1, 1, 4, 0, 0)
        post_period_end = datetime(2022, 1, 1, 19, 0, 0)

        # Must be aware because are used against aware index.
        period_start = datetime(
            2022, 1, 1, 9, 30, 0, tzinfo=pytz.timezone("America/New_York")
        )
        period_end = datetime(
            2022, 1, 1, 16, 0, 0, tzinfo=pytz.timezone("America/New_York")
        )

        # Create pre-market - post-market df.
        index = pd.DatetimeIndex(
            np.arange(
                pre_period_start,
                post_period_end,
                timedelta(minutes=5),
            ).astype(datetime),
            tz="America/New_York",
        )
        rng = np.random.default_rng()

        df = pd.DataFrame(
            {
                "Open": rng.random(len(index)) * 100,
                "High": rng.random(len(index)) * 100,
                "Low": rng.random(len(index)) * 100,
                "Close": rng.random(len(index)) * 100,
                "Volume": rng.integers(10, 20, size=len(index)),
                "Dividends": np.zeros(len(index), np.int8),
                "Stock Split": np.zeros(len(index), np.int8),
            },
            index=index,
        )

        # In case no pre/post market data are requested we slice
        # the df.
        if not kwargs.get("prepost", False):
            df = df[period_start:period_end]
            # Slice market data until "now".
            df = df[: self.now]
            self.open_df = df
        else:
            # Slice market data until "now".
            df = df[: self.now]
            self.df = df

        return df

    def get_item(self):
        """
        Returns random non-coin item.
        """

        return Item.objects.exclude(coin__isnull=False).order_by("?").first()


class TheEyeCoinTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()

        create_items()
        create_user_items()
        self.df = None

    @patch("karpet.Karpet.fetch_crypto_live_data")
    def test_fetch_view(self, mocked_live_data):
        mocked_live_data.side_effect = self.generate_market_data
        item = self.get_item()

        response = self.c.get(reverse("core:the_eye_fetch", args=[item.symbol]))
        response = response.json()

        self.assertTrue(response["status"])
        self.assertEqual(response["data"]["symbol"], item.symbol.upper())
        self.assertEqual(
            response["data"]["min"], autofloatformat(self.df["low"].min(), no_str=True)
        )
        self.assertEqual(
            response["data"]["max"], autofloatformat(self.df["high"].max(), no_str=True)
        )
        self.assertEqual(len(response["data"]["prices"]), len(self.df))

    def generate_market_data(self, *args, **kwargs):
        """
        Creates dataframe with current (fake) market data.

        Emulates karpet.fetch_crypto_live_data() method.

        Generated dataframe has following columns:

        - date time (index)
        - open
        - high
        - low
        - close
        """

        period_start = datetime(2022, 1, 1, 12, 0, 0)
        period_end = datetime(2022, 1, 2, 12, 0, 0)

        # Create df.
        index = pd.DatetimeIndex(
            np.arange(
                period_start,
                period_end,
                timedelta(minutes=30),
            ).astype(datetime),
        )
        rng = np.random.default_rng()

        self.df = pd.DataFrame(
            {
                "open": rng.random(len(index)) * 100,
                "high": rng.random(len(index)) * 100,
                "low": rng.random(len(index)) * 100,
                "close": rng.random(len(index)) * 100,
            },
            index=index,
        )

        return self.df

    def get_item(self):
        """
        Returns random coin item.
        """

        return Item.objects.exclude(coin__isnull=True).order_by("?").first()


class BaseDeleteUserItemTestCase(BaseTestCase):
    def spawn(self):
        create_items()
        item = self.model.objects.get(symbol=self.symbol)
        self.task(item.pk)

        ui_0 = UserItem.objects.create(item=item, user=self.user)
        ui_1 = UserItem.objects.create(item=item, user=self.user2)

        self.assertEqual(UserItem.objects.count(), 2)

        self.assertRedirects(
            self.c.get(reverse(self.url, args=[ui_0.pk])), reverse(self.target_url)
        )

        self.assertEqual(UserItem.objects.count(), 1)
        self.assertEqual(UserItem.objects.first(), ui_1)


class BaseItemOverviewAllocationTestCase(BaseTestCase):
    def prepare(self, prices):
        create_items()
        create_user_items()
        create_prices(prices)

    def spawn(self, transactions, url_pattern):
        create_transactions(transactions)

        response = self.c.get(reverse(url_pattern))
        html = response.content.decode("utf-8")

        doc = BeautifulSoup(html, "html.parser")
        tables = doc.find_all("table")

        # test number of tables
        self.assertEqual(len(tables), 2)

        # test number of meters
        meters = tables[0].find_all("meter")
        self.assertEqual(len(meters), len(transactions))

        # test meters values + order
        allocations = []

        for trans in transactions:
            allocations.append(trans[0]["price"] * trans[0]["amount"])

        total = sum(allocations)
        allocations.sort(reverse=True)

        for i, meter in enumerate(meters):
            self.assertEqual(
                round(float(meter["value"]), 1),
                round(100 / (total / allocations[i]), 1),
            )

        # items from 1st table are not duplicated in 2nd table
        symbols_1 = []
        symbols_2 = []

        for tr in tables[0].find_all("tr"):
            if tr.th:
                continue

            symbols_1.append(tr.td.get_text().strip())

        for tr in tables[1].find_all("tr"):
            if tr.th:
                continue

            symbols_2.append(tr.td.get_text().strip())

        self.assertEqual(set(symbols_1) & set(symbols_2), set())

        # print(tables[0].find_all("tr"))
