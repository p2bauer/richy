from django.forms import FileField

from .widgets import MultipleFileInput


class MultipleFileField(FileField):
    widget = MultipleFileInput

    def __init__(self, delete_url_pattern=None, *args, **kwargs):
        self.delete_url_pattern = delete_url_pattern
        super().__init__(*args, **kwargs)

    def widget_attrs(self, widget):
        attrs = super().widget_attrs(widget)
        attrs["delete_url_pattern"] = self.delete_url_pattern

        return attrs
