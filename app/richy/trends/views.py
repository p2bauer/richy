from datetime import date

from braces.views import FormMessagesMixin, LoginRequiredMixin
from django.contrib import messages
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext as _
from django.views.generic import FormView, RedirectView

from ..core.models import Item, UserItem
from .forms import TrendsForm
from .models import Trend
from .tasks import fetch_trends_chart_data


def one_year_ago():
    today = date.today()
    today.year -= 1

    return today


class TrendsFormMixin:
    def get_trends_form_initial(self):
        initial = {}

        # Google.
        try:
            google = Trend.objects.by_user(self.request.user).get(
                item=self.object.item, network=Trend.GOOGLE
            )
            initial["keywords"] = ", ".join(google.keywords)
            initial["since"] = google.since
        except Trend.DoesNotExist:
            pass

        return initial


class TrendsFormView(LoginRequiredMixin, FormMessagesMixin, TrendsFormMixin, FormView):
    form_class = TrendsForm
    prefix = "trends_form"
    form_valid_message = _("Trends has been saved.")
    form_invalid_message = _("Trends hasn't been saved. Please check the form.")

    def post(self, request, *args, **kwargs):
        self.object = get_object_or_404(UserItem, pk=self.kwargs["pk"])

        return super().post(request, *args, **kwargs)

    def get_initial(self):
        return self.get_trends_form_initial()

    def form_valid(self, form):
        d = form.cleaned_data

        if d.get("keywords"):
            trend, _ = Trend.objects.update_or_create(
                user=self.request.user,
                item=self.object.item,
                network=Trend.GOOGLE,
                defaults={
                    "keywords": d["keywords"],
                    "since": d["since"],
                },
            )

            fetch_trends_chart_data.delay(trend.pk)

        return super().form_valid(form)

    def form_invalid(self, form):
        for e in form.non_field_errors():
            messages.error(self.request, e)

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return self.request.META["HTTP_REFERER"]


class TrendDeleteRedirectView(LoginRequiredMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        Trend.objects.by_user(self.request.user).get(item=kwargs["item_pk"]).delete()
        messages.success(self.request, _("Trend has been deleted."))

        return self.request.META["HTTP_REFERER"]
