from django.contrib import admin

from ..core.admin import ItemMixin
from .models import Etf


@admin.register(Etf)
class EtfAdmin(ItemMixin, admin.ModelAdmin):
    pass
