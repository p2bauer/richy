from django import forms
from django.utils.translation import gettext as _

from .models import Trend


class TrendsForm(forms.ModelForm):
    class Meta:
        model = Trend
        fields = ("keywords", "since")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["since"].widget.attrs["autocomplete"] = "off"
