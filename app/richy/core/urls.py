from django.contrib.auth.views import logout_then_login
from django.urls import path

from . import views

app_name = "core"
urlpatterns = [
    path("sign-in/", views.SignInFormView.as_view(), name="sign_in"),
    path("sign-out/", logout_then_login, name="sign_out"),
    path("search/", views.SearchRedirectView.as_view(), name="search"),
    path("item/<str:slug>/current-price/", views.ItemCurrentPriceAjaxView.as_view()),
    path("the-eye/<str:slug>/", views.TheEyeTemplateView.as_view(), name="the_eye"),
    path(
        "the-eye/<str:slug>/fetch/",
        views.TheEyeAjaxView.as_view(),
        name="the_eye_fetch",
    ),
]
