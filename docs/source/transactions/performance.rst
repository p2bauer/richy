Performance
===========
A class placed in ``transactions.py`` file. It's method ``get_data()``
returns data for Highcharts charts used in Assets section in Transactions
section. Calculates profits or market values over time where the time
is a period a share/coin is held for.

Output structure from ``get_data()`` method:

::

   [
      {
         "name": "BTC",
         "data": [
             [1504656000000, None],  # Timestamp, price
             [1504742400000, None],
             [1504828800000, None],
             [1504915200000, None],
             ...
         ],

      {
         "name": "ETC",
         "data": [
             [1545955200000, 422.4],
             [1546041600000, 415.2],
             [1546128000000, 424.8],
             [1546214400000, 404.0],
             [1546300800000, 418.4]
         ],
      }
      ...
   ]
