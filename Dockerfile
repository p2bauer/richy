FROM node:15-alpine as node

WORKDIR /usr/src

COPY app/richy/core/static .

RUN apk add make g++ python3 && \
    npm i

RUN NODE_ENV=production ./node_modules/webpack-cli/bin/cli.js && rm -rf node_modules


FROM python:3.11-slim
ARG REQUIREMENTS_FILE=requirements

RUN apt update && apt install -y \
    libxml2-dev libxslt-dev libjpeg-dev \
    gcc git \
    fonts-roboto fonts-freefont-ttf \
    graphviz \
    && rm -rf /var/cache/apk/*

# Copy everything necessary to the image.
COPY app /var/www/app

# Copy compiled static files from node temporary image.
COPY --from=node /usr/src /var/www/app/richy/core/static

WORKDIR /var/www/app

# Set up app environment.
RUN pip3 install -r ${REQUIREMENTS_FILE} --no-cache-dir

CMD ["./startup.sh"]
